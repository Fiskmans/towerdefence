﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Tower_Defence
{
    class world
    {
        public tile[][] tiles;


        public world(int size)
        {
            tiles = new tile[size][];
            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i] = new tile[size];
                for (int j = 0; j < tiles[i].Length; j++)
                {
                    tiles[i][j] = new tile(new Point(i,j));
                }
            }
        }

        public void Update(MouseState ms,MouseState oldms,GameTime gt)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                for (int j = 0; j < tiles[i].Length; j++)
                {
                    tiles[i][j].PreUpdate(FindAdjecent(i, j),ms,oldms,gt);
                }
            }
            for (int i = 0; i < tiles.Length; i++)
            {
                for (int j = 0; j < tiles[i].Length; j++)
                {
                    tiles[i][j].Update(FindAdjecent(i,j),ms,oldms,gt);
                }
            }
            for (int i = 0; i < tiles.Length; i++)
            {
                for (int j = 0; j < tiles[i].Length; j++)
                {
                    tiles[i][j].PostUpdate(FindAdjecent(i, j),ms,oldms,gt);
                }
            }
        }

        public void Draw(SpriteBatch s)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                for (int j = 0; j < tiles[i].Length; j++)
                {
                    tiles[i][j].Draw(s);
                }
            }
        }

        public tile[] FindAdjecent(int x,int y)
        {
            tile[] _tiles = new tile[8];
            Point[] points = new Point[] { new Point(0, -1), new Point(1, -1), new Point(1, 0), new Point(1, 1), new Point(0, 1), new Point(-1, 1), new Point(-1, 0), new Point(-1, -1) };

            for (int i = 0; i < points.Length; i++)
            {
                if (x + points[i].X >= 0 && x + points[i].X < tiles.Length && y + points[i].Y >= 0 && y + points[i].Y < tiles[x + points[i].X].Length)
                {
                    _tiles[i] = (tiles[x+points[i].X][y+points[i].Y]);
                }
            }
            return _tiles.ToArray();
        }
    }
}
