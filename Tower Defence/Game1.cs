﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Diagnostics;

namespace Tower_Defence
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public static Texture2D tiledirt;
        public static Texture2D target;
        public static Texture2D border;
        float animate = 0;
        Stopwatch sw;
        public static Point TileSize = new Point(64, 64);
        public const int TimingConstant = 1000; //one time unit

        world w;
        MouseState oldms = Mouse.GetState();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            IsMouseVisible = true;
            sw = new Stopwatch();
            sw.Start();
            w = new world(5);
            w.tiles[0][0].tilestate = tile.state.conduit;
            base.Initialize();
        }
        
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            tiledirt = Content.Load<Texture2D>("tiledirt");
            target = Content.Load<Texture2D>("highlight");
            border = Content.Load<Texture2D>("border");
        }
        
        protected override void UnloadContent()
        {
        }
        
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) { Exit(); }
            w.tiles[0][0].value = 10;
            w.Update(Mouse.GetState(),oldms, gameTime);
            base.Update(gameTime);
        }
        
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            w.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
            
        }

        public static void DrawFromTileSheet(SpriteBatch s,Texture2D Tilesheet, Vector2 tilesize,Vector2 displacement,int width,int x,int y,Vector2 pos,Color c)
        {
            s.Draw(Tilesheet,pos,new Rectangle((int)(displacement.X + tilesize.X * (x%width)),(int)(displacement.Y + tilesize.Y * (y + (int)(x/width))),(int)(tilesize.X),(int)(tilesize.Y)),c);
        }
    }
}
