﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Tower_Defence
{
    class tile
    {
        Point Position;
        public enum state { block,conduit };
        public state tilestate = tile.state.block;
        public int value = 0;
        public int toughness = 10;
        public int timer;

        bool decrease = false;
        bool highlighted = false;
        bool digging = false;

        public tile(Point position)
        {
            this.Position = position;
        }


        public void Draw(SpriteBatch s)
        {
            switch (tilestate)
            {
                case state.block:
                    Game1.DrawFromTileSheet(s, Game1.tiledirt, new Vector2(64), Vector2.Zero, 10, value, 0, new Vector2(Position.X*Game1.TileSize.X, Position.Y*Game1.TileSize.Y),Color.White);
                    if (digging)
                    {
                        s.Draw(Game1.border, new Vector2(Position.X * Game1.TileSize.X, Position.Y * Game1.TileSize.Y), Color.White);
                    }
                    break;
                case state.conduit:
                    //s.Draw()
                    break;
                default:
                    break;
            }
            if (highlighted)
            {
                s.Draw(Game1.target, new Vector2(Position.X * Game1.TileSize.X, Position.Y * Game1.TileSize.Y), Color.White);
            }
        }

        public void PreUpdate(tile[] tiles,MouseState ms,MouseState oldms,GameTime gt)
        {
            timer += gt.ElapsedGameTime.Milliseconds;
            highlighted = false;

            
        }

        public void Update(tile[] tiles, MouseState ms, MouseState oldms, GameTime gt)
        {
            if (value >= 9)
            {
                decrease = false;
            }
            if (timer >= Game1.TimingConstant)
            {
                timer -= Game1.TimingConstant;
                if (digging)
                {
                    int avpower = 0;
                    foreach (tile t in tiles)
                    {
                        if (t != null && t.tilestate == state.conduit && t.value > 0)
                        {
                            avpower += t.value;
                        }
                    }
                    if (avpower > toughness)
                    {
                        foreach (tile t in tiles)
                        {
                            if (t != null && t.tilestate == state.conduit && t.value > 0)
                            {
                                avpower += t.value;
                            }
                        }
                    }
                    this.value += 1;
                }
            }
            if (new Rectangle(Position*Game1.TileSize,Game1.TileSize).Contains(ms.X,ms.Y))
            {
                if (ms.LeftButton == ButtonState.Pressed && oldms.LeftButton == ButtonState.Released)
                {
                    digging = true;
                }

                highlighted = true;
            }
        }

        public void PostUpdate(tile[] tiles, MouseState ms, MouseState oldms, GameTime gt)
        {

        }
    }
}
